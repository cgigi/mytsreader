#pragma once

#include <bitset>
#include <cstdint>
#include <iostream>
#include <sstream>
#include <memory.h>
#include <vector>

#include <cstdint>
#include <cstring>
#include <iostream>

#include "adaptationField.hpp"
#include "PacketHeader.hpp"
#include "PESPacketHeader.hpp"
#include "utils.hpp"


class PESAssembler {
    public:
        enum class eResult : int32_t {
            UnexpectedPID = 1,
            StreamPackedLost,
            AssemblingStarted,
            AssemblingContinue,
            AssemblingFinished,
        };

        PESAssembler(const uint8_t pid);
        ~PESAssembler();

        eResult parse(const uint8_t *TSPacket, const PacketHeader *TSPacketHeader, const adaptationField *TSAdaptationField);
        bool continuityCheck(const uint8_t CC) const;

        int32_t get_header_length() const;
        int32_t get_data_length() const;
        int32_t get_packet_length() const;
        int get_stream_id() const;

        void xBufferReset();
        void xBufferAppend(const uint8_t *data, int32_t size);

        std::string dumpPESHeader() const;
        
    protected:
        int32_t PID;

        uint8_t *buffer;
        uint32_t bufferSize;
        uint32_t dataOffset;
        uint8_t clear_buffer;

        int8_t lastContinuityCounter;
        bool started;
        PESPacketHeader pesPacketHeader;
        FILE *file;
        std::string file_extension;
};
