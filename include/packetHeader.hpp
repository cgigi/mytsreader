#pragma once

#include <bitset>
#include <cstdint>
#include <iostream>
#include <sstream>
#include <vector>

#include "utils.hpp"

class PacketHeader {
    public:
        PacketHeader();
        ~PacketHeader();
        
        void parse(const uint8_t* buffer);

        unsigned long int get_sync_byte() const;
        unsigned long int get_transport_error_indicator() const;
        unsigned long int get_pid() const;
        unsigned long int get_adaptation_field_control() const;
        unsigned long int get_continuity_counter() const;
        unsigned long int get_payload_unit_start_indicator() const;
        bool has_adaptation_field() const;
    protected:
        std::bitset<8>  sync_byte;
        std::bitset<1>  transport_error_indicator;
        std::bitset<1>  payload_unit_start_indicator;
        std::bitset<1>  transport_priority;
        std::bitset<13> PID;
        std::bitset<2>  transport_scrambling_control;
        std::bitset<2>  adaptation_field_control;
        std::bitset<4>  continuity_counter;
};
