#pragma once

#include <bitset>
#include <cstdint>
#include <unordered_map>
#include <vector>

#include "logger.hpp"
#include "file_io.hpp"
#include "PacketHeader.hpp"
#include "adaptationField.hpp"
#include "PESAssembler.hpp"


class TsParser
{
    public:
        TsParser(std::string path_file, logger::Logger logger);
        ~TsParser();
        
        bool parse();
    private:
        std::string m_path_file;
        logger::Logger m_logger;
        int32_t currentPacket;
        std::unordered_map<uint16_t, PESAssembler> assemblers;
};
