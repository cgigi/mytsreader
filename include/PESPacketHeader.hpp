#pragma once

#include <bitset>
#include <cstdint>
#include <iostream>
#include <sstream>
#include <vector>

#include "utils.hpp"


class PESPacketHeader {
    public:
        PESPacketHeader();
        ~PESPacketHeader();

        void parse(const uint8_t *buffer);
        void wipe(std::basic_stringstream<char, std::char_traits<char>, std::allocator<char>> *stream) const;
        std::bitset<33> connectBitsets(std::bitset<3> first, std::bitset<15> second, std::bitset<15> third);
        std::string dump() const;

        int16_t get_packet_length() const;
        uint8_t get_header_data_length() const;
        uint8_t get_stream_id() const;

    protected:
        uint8_t headerLength;
        std::bitset<24> packet_start_code_prefix;
        std::bitset<8> stream_id;
        std::bitset<16> PES_packet_length;
        std::bitset<2> PES_scrambling_control;
        std::bitset<1> PES_priority;
        std::bitset<1> data_alignment_indicator;
        std::bitset<1> copyright;
        std::bitset<1> original_or_copy;
        std::bitset<2> PTS_DTS_flags;
        std::bitset<1> ESCR_flag;
        std::bitset<1> ES_rate_flag;
        std::bitset<1> DSM_trick_mode_flag;
        std::bitset<1> additional_copy_info_flag;
        std::bitset<1> PES_CRC_flag;
        std::bitset<1> PES_extension_flag;
        std::bitset<8> PES_header_data_length;
        std::bitset<33> PTS;
        std::bitset<33> DTS;
        bool is_PTS;
        bool is_DTS;
};
