#pragma once

#include <algorithm>
#include <cstdint>
#include <string>

static constexpr uint32_t TS_PacketLength = 188;
static constexpr uint32_t TS_HeaderLength = 4;
static constexpr uint32_t PES_HeaderLength = 6;

std::string getBinaryRepresentation(const uint8_t *input, int start, int count);
