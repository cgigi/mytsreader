#include "adaptationField.hpp"

adaptationField::adaptationField() {
    stuffing_count = 0;
}

adaptationField::~adaptationField() {}

void adaptationField::parse(const uint8_t *buffer) {
    adaptation_field_length = buffer[4];
    std::stringstream stream_adpation_field(getBinaryRepresentation(buffer, 5, adaptation_field_length.to_ulong()));
    auto bytes_offset = 1;
    auto t = stuffing_count;
    if (adaptation_field_length.to_ulong() > 0) {
        stream_adpation_field >> discontinuity_indicator;
        stream_adpation_field >> random_access_indicator;
        stream_adpation_field >> elementary_stream_priority_indicator;
        stream_adpation_field >> PCR_flag;
        stream_adpation_field >> OPCR_flag;
        stream_adpation_field >> splicing_point_flag;
        stream_adpation_field >> transport_private_data_flag;
        stream_adpation_field >> adaptation_field_extension_flag;

        if (PCR_flag == '1') {
            stream_adpation_field >> PCR_base;
            stream_adpation_field >> PCR_reserved;
            stream_adpation_field >> PCR_extension;
            bytes_offset += 6;
        }
        if (OPCR_flag == '1') {
            stream_adpation_field >> OPCR_base;
            stream_adpation_field >> OPCR_reserved;
            stream_adpation_field >> OPCR_extension;
            bytes_offset += 6;
        }
        if (splicing_point_flag == '1') {
            stream_adpation_field >> splice_countdown;
            bytes_offset++;
        }
        if (transport_private_data_flag == '1') {
            stream_adpation_field >> transport_private_data_length;
            private_data_byte = new uint8_t[transport_private_data_length.to_ulong()];
            stream_adpation_field >> private_data_byte;
            bytes_offset += transport_private_data_length.to_ulong();
        }
        if (adaptation_field_extension_flag == '1') {
            stream_adpation_field >> adaptation_field_extension_length;
            stream_adpation_field >> ltw_flag;
            stream_adpation_field >> piecewise_rate_flag;
            stream_adpation_field >> seamless_splice_flag;
            stream_adpation_field >> reserved_extension_1;
            bytes_offset += adaptation_field_extension_length.to_ulong();
            if (ltw_flag == '1') {
                stream_adpation_field >> ltw_valid_flag;
                stream_adpation_field >> ltw_offset;
                bytes_offset += 2;
            }
            if (piecewise_rate_flag == '1') {
                stream_adpation_field >> reserved_extension_2;
                stream_adpation_field >> piecewise_rate;
                bytes_offset += 3;
            }
            if (seamless_splice_flag == '1') {
                stream_adpation_field >> splice_type;
                stream_adpation_field >> DTS_next_AU_32_30;
                stream_adpation_field >> marker_bit_30;
                stream_adpation_field >> DTS_next_AU_29_15;
                stream_adpation_field >> marker_bit_15;
                stream_adpation_field >> marker_bit;
                bytes_offset += 5;
            }
            stuffing_count = adaptation_field_length.to_ulong() - bytes_offset;
            t = t;
        }
    }
    stream_adpation_field.str(std::string());
}

std::string adaptationField::dump() const {
    std::stringstream payload_adaptation_field;
    payload_adaptation_field << "Adapatation field:\n";
    payload_adaptation_field << "length: " << adaptation_field_length.to_ulong() << '\n';
    payload_adaptation_field << "discontinuity indicator: " << discontinuity_indicator.to_ulong() << '\n';
    payload_adaptation_field << "\trandom access indicator: " << random_access_indicator.to_ulong() << '\n';
    payload_adaptation_field << "\tstream priority indicator: " << elementary_stream_priority_indicator.to_ulong() << '\n',
    payload_adaptation_field << "\tPCR flag: " << PCR_flag.to_ulong() + '\n';
    if (PCR_flag == '1') {
        auto PCR_val = (PCR_base.to_ulong() * 300) + PCR_extension.to_ulong();
        // auto time = (double(PCR_val) / double(27000000));
        payload_adaptation_field << "\t\tPCR: " << PCR_val + '\n';
    }

    payload_adaptation_field << "\tOPCR flag: " << OPCR_flag.to_ulong() << '\n';
    if (OPCR_flag == '1') {
        auto OPCR_val = (OPCR_base.to_ulong() * 300) + OPCR_extension.to_ulong();
        // auto time = (double(OPCR_val) / double(27000000));
        payload_adaptation_field << "\t\tOPCR: " << OPCR_val << '\n';
    }
    payload_adaptation_field << "\tsplicing point flag: " << splicing_point_flag.to_ulong() << '\n';
    payload_adaptation_field << "\ttransport private data flag: " << transport_private_data_flag.to_ulong() << '\n';
    payload_adaptation_field << "\tadaptation field extension: " << adaptation_field_extension_flag.to_ulong() << '\n';
    payload_adaptation_field << "\tstuffing: " << stuffing_count;
    return payload_adaptation_field.str();
}

uint8_t adaptationField::get_adaptation_field_length() const {
    return adaptation_field_length.to_ulong();
}
