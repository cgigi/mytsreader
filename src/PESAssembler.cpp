#include "PESAssembler.hpp"

PESAssembler::PESAssembler(const uint8_t pid) : PID(pid) {
    lastContinuityCounter = -1;
    bufferSize = 0;
    started = false;
    dataOffset = PES_HeaderLength;
    uint8_t clear_buffer = 0;
    buffer = &clear_buffer;

    switch (PID) {
        case 136:
            file_extension = ".mp2";
            break;
        case 174:
            file_extension = ".264";
            break;
        default:
            file_extension = ".bin";
            break;
    }

    std::string file_name = std::string("outputs/PID_" + std::to_string(PID) + file_extension);
    file = fopen(file_name.c_str(), "wb");
}


PESAssembler::~PESAssembler() {}

PESAssembler::eResult PESAssembler::parse(const uint8_t *TSPacket, const PacketHeader *TSPacketHeader, const adaptationField *TSAdaptationField) {
    int start, payload;
    if (continuityCheck(TSPacketHeader->get_continuity_counter())) {
        xBufferReset();
        return eResult::StreamPackedLost;
    }

    if (TSPacketHeader->get_payload_unit_start_indicator() == 1) {
        if (started && pesPacketHeader.get_packet_length() == 0) {
            started = false;
            fwrite(&buffer[this->get_header_length()], this->get_header_length(), 1, file);
            xBufferReset();
        }

        if (buffer != nullptr) xBufferReset();

        lastContinuityCounter = TSPacketHeader->get_continuity_counter();
        started = true;

        start = TS_HeaderLength + TSAdaptationField->get_adaptation_field_length() + 1;
        payload = TS_PacketLength - TS_HeaderLength - (TSAdaptationField->get_adaptation_field_length() + 1);

        if (TSAdaptationField->get_adaptation_field_length() == 0) {
            start--;
            payload++;
        }

        pesPacketHeader.parse(&TSPacket[start]);

        xBufferAppend(&TSPacket[start], payload);
        return eResult::AssemblingStarted;
    }

    if (started) {
        lastContinuityCounter = TSPacketHeader->get_continuity_counter();

        if (TSPacketHeader->has_adaptation_field()) {
            start = TS_HeaderLength + TSAdaptationField->get_adaptation_field_length() + 1;
            payload = TS_PacketLength - TS_HeaderLength - (TSAdaptationField->get_adaptation_field_length() + 1);
        } else {
            payload = TS_PacketLength - TS_HeaderLength;
            start = TS_HeaderLength;
        }

        xBufferAppend(&TSPacket[start], payload);

        if (pesPacketHeader.get_packet_length() + PES_HeaderLength == bufferSize) {
            started = false;
            fwrite(&buffer[this->get_header_length()], this->get_data_length(), 1, file);
            return eResult::AssemblingFinished;
        }
        return eResult::AssemblingContinue;
    }
    return eResult::UnexpectedPID;
}

bool PESAssembler::continuityCheck(const uint8_t CC) const {
    return (lastContinuityCounter != -1 && (CC - lastContinuityCounter != 1 && CC - lastContinuityCounter != -15));   
}

int32_t PESAssembler::get_header_length() const {
    return pesPacketHeader.get_header_data_length() + 6;
}

int32_t PESAssembler::get_data_length() const {
    return dataOffset - this->get_header_length();
}

int32_t PESAssembler::get_packet_length() const {
    return dataOffset;;
}

int PESAssembler::get_stream_id() const {
    return (int)(pesPacketHeader.get_stream_id());
}

void PESAssembler::xBufferReset() {
    buffer = nullptr;
    bufferSize = 0;
    dataOffset = 0;
}

void PESAssembler::xBufferAppend(const uint8_t *data, int32_t size) {
    dataOffset += size;
    bufferSize += size;

    if (buffer == nullptr) {
        buffer = new uint8_t[bufferSize];
        std::memcpy(buffer, data, bufferSize);
        return;
    }
    uint8_t *temp_buffer = new uint8_t[bufferSize];

    std::memcpy(temp_buffer, buffer, bufferSize - size);
    std::memcpy(&temp_buffer[bufferSize - size], data, size);

    delete[] buffer;
    buffer = new uint8_t[bufferSize];
    std::memcpy(buffer, temp_buffer, bufferSize);
    delete[] temp_buffer;    
}

std::string PESAssembler::dumpPESHeader() const {
    return pesPacketHeader.dump();
}
