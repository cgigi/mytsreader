#include "PacketHeader.hpp"


PacketHeader::PacketHeader() {}

PacketHeader::~PacketHeader() {}

void PacketHeader::parse(const uint8_t* buffer)
{
    std::stringstream header_packet(getBinaryRepresentation(buffer, 0, 188));
    header_packet >> sync_byte;
    header_packet >> transport_error_indicator;
    header_packet >> payload_unit_start_indicator;
    header_packet >> transport_priority;
    header_packet >> PID;
    header_packet >> transport_scrambling_control;
    header_packet >> adaptation_field_control;
    header_packet >> continuity_counter;
}

unsigned long int PacketHeader::get_sync_byte() const {
    return sync_byte.to_ulong();
}

unsigned long int PacketHeader::get_transport_error_indicator() const {
    return transport_error_indicator.to_ulong();
}

unsigned long int PacketHeader::get_pid() const {
    return PID.to_ulong();
}

unsigned long int PacketHeader::get_adaptation_field_control() const {
    return adaptation_field_control.to_ulong();
}

unsigned long int PacketHeader::get_continuity_counter() const {
    return continuity_counter.to_ulong();
}

unsigned long int PacketHeader::get_payload_unit_start_indicator() const {
    return payload_unit_start_indicator.to_ulong();
}

bool PacketHeader::has_adaptation_field() const {
    return adaptation_field_control == 2 || adaptation_field_control == 3;
}
