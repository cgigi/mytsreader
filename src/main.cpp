#include <iostream>

#include "logger.hpp"
#include "parser.hpp"

int main() {
    auto logger = logger::Logger("test.log", 5, true);
    auto parser = TsParser("C:/Users/ginan/Videos/work/sample-1.ts", logger);
    
    if (!parser.parse()) {
        logger.add_log(logger::Logger::MessageType::FATAL, "Error while parsing input file");
        return -1;
    }
    logger.add_log(logger::Logger::MessageType::INFO, "Done");
    return 0;
}
