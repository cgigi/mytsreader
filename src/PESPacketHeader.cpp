#include "PESPacketHeader.hpp"


PESPacketHeader::PESPacketHeader() {}

PESPacketHeader::~PESPacketHeader() {}

void PESPacketHeader::parse(const uint8_t *buffer) {
    std::stringstream stream_pes(getBinaryRepresentation(buffer, 0, TS_PacketLength));
    stream_pes >> packet_start_code_prefix;
    stream_pes >> stream_id;
    stream_pes >> PES_packet_length;

    headerLength += 6;
    //
    // streamId_program_stream_map = 0xBC,
    // streamId_padding_stream = 0xBE,
    // streamId_private_stream_2 = 0xBF,
    // streamId_ECM = 0xF0,
    // streamId_EMM = 0xF1,
    // streamId_program_stream_directory = 0xFF,
    // streamId_DSMCC_stream = 0xF2,
    // streamId_ITUT_H222_1_type_E = 0xF8,
    //
    auto SID = stream_id.to_ulong();
    if (SID != 0xBC && SID != 0xBE && SID != 0xBF &&
        SID != 0xF0 && SID != 0xF1 && SID != 0xFF &&
        SID != 0xF2 && SID != 0xF8) {

        this->wipe(&stream_pes);
        stream_pes << getBinaryRepresentation(buffer, PES_HeaderLength, 3);

        stream_pes.ignore(2);
        stream_pes >> PES_scrambling_control;
        stream_pes >> PES_priority;
        stream_pes >> data_alignment_indicator;
        stream_pes >> copyright;
        stream_pes >> original_or_copy;
        stream_pes >> PTS_DTS_flags;
        stream_pes >> ESCR_flag;
        stream_pes >> ES_rate_flag;
        stream_pes >> DSM_trick_mode_flag;
        stream_pes >> additional_copy_info_flag;
        stream_pes >> PES_CRC_flag;
        stream_pes >> PES_extension_flag;
        stream_pes >> PES_header_data_length;
        this->wipe(&stream_pes);

        stream_pes << getBinaryRepresentation(buffer, TS_PacketLength + 3, PES_header_data_length.to_ulong());
        if (PTS_DTS_flags.to_ulong() == 2) {
            stream_pes.ignore(4);
            std::bitset<3> PTS_32_30;
            stream_pes >> PTS_32_30;
            stream_pes.ignore(1);
            std::bitset<15> PTS_29_15;
            stream_pes >> PTS_29_15;
            stream_pes.ignore(1);
            std::bitset<15> PTS_14_0;
            stream_pes >> PTS_14_0;
            stream_pes.ignore(1);
            PTS = connectBitsets(PTS_32_30, PTS_29_15, PTS_14_0);
            is_PTS = true; 
        }

        if (PTS_DTS_flags.to_ulong() == 3) {
            stream_pes.ignore(4);
            std::bitset<3> PTS_32_30;
            stream_pes >> PTS_32_30;
            stream_pes.ignore(1);
            std::bitset<15> PTS_29_15;
            stream_pes >> PTS_29_15;
            stream_pes.ignore(1);
            std::bitset<15> PTS_14_0;
            stream_pes >> PTS_14_0;
            stream_pes.ignore(1);
            stream_pes.ignore(4);
            std::bitset<3> DTS_32_30;
            stream_pes >> DTS_32_30;
            stream_pes.ignore(1);
            std::bitset<15> DTS_29_15;
            stream_pes >> DTS_29_15;
            stream_pes.ignore(1);
            std::bitset<15> DTS_14_0;
            stream_pes >> DTS_14_0;
            stream_pes.ignore(1);
            PTS = connectBitsets(PTS_32_30, PTS_29_15, PTS_14_0);
            is_PTS = true;
            DTS = connectBitsets(DTS_32_30, DTS_29_15, DTS_14_0);
            is_DTS = true;            
        }
        this->wipe(&stream_pes);
    }
}


void PESPacketHeader::wipe(std::basic_stringstream<char, std::char_traits<char>, std::allocator<char>> *stream) const {
    stream->str(std::string());
    stream->clear();
}


std::bitset<33> PESPacketHeader::connectBitsets(std::bitset<3> first, std::bitset<15> second, std::bitset<15> third) {
    std::string value = (first.to_string() + second.to_string() + third.to_string());
    return std::bitset<33>(value);
}


std::string PESPacketHeader::dump() const {
    std::stringstream output;
    output << "PES:\n";
    output << "SID: " + stream_id.to_ulong() << '\n';
    output << "PSCP: " + packet_start_code_prefix.to_ulong() << '\n';
    output << " L: " + PES_packet_length.to_ulong() << '\n';

    if (is_PTS)
        output << " PTS: " << PTS.to_ulong() << '\n';
    if (is_DTS)
        output << " DTS: " << DTS.to_ulong() << '\n';
    return output.str();
}

int16_t PESPacketHeader::get_packet_length() const {
    return PES_packet_length.to_ulong();
}

uint8_t PESPacketHeader::get_header_data_length() const {
    return 3 + PES_header_data_length.to_ulong();
}

uint8_t PESPacketHeader::get_stream_id() const {
    return stream_id.to_ulong();
}
