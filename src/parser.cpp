#include "parser.hpp"


TsParser::TsParser(std::string path_file, logger::Logger logger) : m_path_file(path_file), m_logger(logger)
{
    currentPacket = 0;
}

TsParser::~TsParser()
{}

bool TsParser::parse()
{
    PacketHeader packet_header;
    if (!file_exists(m_path_file))
    {
        m_logger.add_log(logger::Logger::MessageType::FATAL, "Unable to open the TS file");
        m_logger.add_log(logger::Logger::MessageType::FATAL, "No such file or directory");
        return false;
    }
    auto file_input = fopen(m_path_file.c_str(), "rb");
    uint8_t buffer[TS_PacketLength];
    while (!feof(file_input))
    {
        auto packet_entry = fread(&buffer, 1, TS_PacketLength, file_input);
        if (packet_entry != TS_PacketLength)
            break;

        packet_header.parse(buffer);
        if (static_cast<char>(packet_header.get_sync_byte()) != 'G')
        {
            m_logger.add_log(logger::Logger::MessageType::ERROR, "SyncByte of packet is invalid");
            continue;
        }
        if (packet_header.get_transport_error_indicator())
            m_logger.add_log(logger::Logger::MessageType::WARN, "Transport error detected of packet");

        auto current_pid = packet_header.get_pid();
        m_logger.add_log(logger::Logger::MessageType::INFO, "PID: " + std::to_string(current_pid));
        auto adaptation_field_packet = adaptationField();
        if (packet_header.get_adaptation_field_control() == 2 || packet_header.get_adaptation_field_control() == 3) {
            adaptation_field_packet.parse(buffer);
            std::bitset<8> test;
            test = buffer[4];
            auto payload_adaptation_field = adaptation_field_packet.dump();
            m_logger.add_log(logger::Logger::MessageType::INFO, payload_adaptation_field);
        }

        if (assemblers.find(current_pid) == assemblers.end())
            assemblers.insert(std::pair<uint16_t, PESAssembler>{current_pid, PESAssembler(uint8_t(current_pid))});
        auto assembler = &assemblers.at(current_pid);
        auto result = assembler->parse(buffer, &packet_header, &adaptation_field_packet);
        std::stringstream ss_assembler;
        switch (result) {
            case PESAssembler::eResult::StreamPackedLost : {
                ss_assembler << " PcktLost SID=" << assembler->get_stream_id() << " ";
                break;
            }
            case PESAssembler::eResult::AssemblingStarted : {
                ss_assembler << " Started SID=" << assembler->get_stream_id() << " ";
                auto dumpPESHeader = assembler->dumpPESHeader();
                ss_assembler << dumpPESHeader;
                break;
            }
            case PESAssembler::eResult::AssemblingContinue: {
                ss_assembler << " Continue SID=" << assembler->get_stream_id() << " ";
                break;
            }
            case PESAssembler::eResult::AssemblingFinished: {
                ss_assembler << " Finished SID=" << assembler->get_stream_id() << " ";
                {
                    ss_assembler << "PES: PcktLen=" << assembler->get_packet_length();
                    ss_assembler << " HeadLen=" << assembler->get_header_length();
                    ss_assembler << " DataLen=" << assembler->get_data_length();
                    break;
                }
            }
            default: {
                break;
            }
        }
        m_logger.add_log(logger::Logger::MessageType::INFO, ss_assembler.str());
        currentPacket++;
    }
    fclose(file_input);
    return true;
}
